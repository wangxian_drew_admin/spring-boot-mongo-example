FROM hub.c.163.com/library/java:latest
MAINTAINER WangXian
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
ENV LANG=zh_CN.UTF-8 \
    LC_ALL=zh_CN.UTF-8
VOLUME /tmp
ADD target/application-1.0.jar app.jar
ENTRYPOINT ["java","-jar","-Dfile.encoding=utf-8","-Dspring.profiles.active=local","/app.jar"]
