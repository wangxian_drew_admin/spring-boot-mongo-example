echo '###################开始构建######################'
pwd
echo '###################构建Jar包####################'
mvn clean install -Dmaven.test.skip

echo '##################构建Docker Image####################'

docker stop demo_mongo
docker rm demo_mongo

docker rmi demo_mongo

docker build -t demo_mongo .

docker run -d -p 9090:9090 --name demo_mongo demo_mongo
