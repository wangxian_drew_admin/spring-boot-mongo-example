package org.yan.mongo.entity.shp;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.Map;

/**
 * 基础Shape类
 *
 * @author wangx 2020/06/15 14:21
 */
@Data
public class BaseShapeModel {

    @MongoId
    private String _id;

    private String id;

    private String type;


    private Map<String, Object> properties;

}
