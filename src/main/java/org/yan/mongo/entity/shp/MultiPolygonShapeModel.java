package org.yan.mongo.entity.shp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * Model
 *
 * @author wangx 2020/06/15 9:12
 */
@EqualsAndHashCode(callSuper = true)
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Document(value = "park_guanfumian_data")
public class MultiPolygonShapeModel extends BaseShapeModel implements Serializable {

    private GeometryModel geometry;

}
