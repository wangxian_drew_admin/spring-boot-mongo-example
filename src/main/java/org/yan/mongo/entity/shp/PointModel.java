package org.yan.mongo.entity.shp;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 点数据
 *
 * @author wangx 2020/06/15 14:47
 */
@Data
public class PointModel {
    private String type;

    private List<BigDecimal> coordinates;

}
