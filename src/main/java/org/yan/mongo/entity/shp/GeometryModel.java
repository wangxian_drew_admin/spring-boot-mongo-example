package org.yan.mongo.entity.shp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 几何数据
 *
 * @author wangx 2020/06/15 11:19
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GeometryModel {

    private String type;

    private List<List<List<List<BigDecimal>>>> coordinates;
}
