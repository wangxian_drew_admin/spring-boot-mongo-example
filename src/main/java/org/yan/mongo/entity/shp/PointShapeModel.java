package org.yan.mongo.entity.shp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * 点shape数据
 *
 * @author wangx 2020/06/15 14:22
 */
@EqualsAndHashCode(callSuper = true)
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Document(value = "park_treetop_data")
public class PointShapeModel extends BaseShapeModel implements Serializable {

    private PointModel geometry;
}
