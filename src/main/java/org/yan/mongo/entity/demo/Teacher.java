package org.yan.mongo.entity.demo;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.List;

/**
 * @author wangx 2020/6/8 17:04
 */
@Document(collection = "teacher")
@Data
public class Teacher {

    @MongoId
    private String id;


    private String name;

    private String sex;

    private Integer age;

    private List<Student> students;
}
