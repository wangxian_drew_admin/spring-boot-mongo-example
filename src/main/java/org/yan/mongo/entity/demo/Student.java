package org.yan.mongo.entity.demo;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

/**
 * @author wangx 2020/6/8 16:47
 */

@Document(collection = "student")
@Data
public class Student {
    @MongoId
    private String id;

    private String name;

    private String sex;

    private Integer age;
}
