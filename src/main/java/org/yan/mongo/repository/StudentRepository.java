package org.yan.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.yan.mongo.entity.demo.Student;

/**
 * 继承MongoRepository,便实现了增删改查等基础功能
 *
 * @author wangx 2020/6/8 16:50
 */
public interface StudentRepository extends MongoRepository<Student, String> {
}
