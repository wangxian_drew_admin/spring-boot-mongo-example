package org.yan.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.yan.mongo.entity.demo.Teacher;

/**
 * @author wangx 2020/6/8 17:04
 */
public interface TeacherRepository extends MongoRepository<Teacher, String> {
}
