package org.yan.mongo.controller.demo;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.yan.mongo.entity.demo.Student;
import org.yan.mongo.service.StudentService;

import java.util.List;

/**
 * 学生API
 *
 * @author wangx 2020/6/8 16:51
 */

@RestController
@RequestMapping("/api/v1/students")
@AllArgsConstructor
public class StudentController {
    private final StudentService studentService;


    @PostMapping
    public ResponseEntity<Student> save(@RequestBody Student student) {

        Student result = studentService.save(student);

        return ResponseEntity.ok(result);
    }


    @PatchMapping
    public ResponseEntity<Student> update(@RequestBody Student student) {
        Student result = studentService.save(student);
        return ResponseEntity.ok(result);
    }

    @GetMapping
    public ResponseEntity<List<Student>> listAll() {
        return ResponseEntity.ok(studentService.listAll());
    }


}
