package org.yan.mongo.controller.demo;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.yan.mongo.entity.demo.Teacher;
import org.yan.mongo.service.TeacherService;

import java.util.List;

/**
 * 教师API,简单增删改查
 *
 * @author wangx 2020/6/8 17:05
 */
@RestController
@RequestMapping("/api/v1/teachers")
@AllArgsConstructor
public class TeacherController {

    private final TeacherService teacherService;


    @PostMapping
    public ResponseEntity<Teacher> save(@RequestBody Teacher teacher) {
        Teacher result = teacherService.save(teacher);
        return ResponseEntity.ok(result);
    }

    @PatchMapping
    public ResponseEntity<Teacher> update(@RequestBody Teacher teacher) {
        Teacher result = teacherService.save(teacher);
        return ResponseEntity.ok(result);
    }

    @GetMapping
    public ResponseEntity<List<Teacher>> listAll() {
        return ResponseEntity.ok(teacherService.listAll());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") String id) {
        teacherService.delete(id);
        return ResponseEntity.ok("删除成功");
    }
}
