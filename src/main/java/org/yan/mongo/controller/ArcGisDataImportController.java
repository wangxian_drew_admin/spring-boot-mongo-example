package org.yan.mongo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.yan.mongo.entity.shp.BaseShapeModel;
import org.yan.mongo.util.ShapeToolUtils;

import java.io.File;
import java.util.List;

/**
 * ArcGis数据导入
 *
 * @author wangx 2020/06/14 22:01
 */

@RestController
@RequestMapping("/arc_gis/data/import")
public class ArcGisDataImportController {
    @Autowired
    MongoTemplate mongoTemplate;

    @GetMapping("/shp")
    public ResponseEntity<List<BaseShapeModel>> importShapeFile() throws Exception {
        List<BaseShapeModel> multiPolygonShapeModels = ShapeToolUtils.getShapeFile(new File("C:\\Users\\wangx\\Documents\\Dshubs\\省林业局Gis项目\\矢量数据\\实验林场数据\\树高点数据.shp"));
        mongoTemplate.insertAll(multiPolygonShapeModels);
        return ResponseEntity.ok(multiPolygonShapeModels);
    }
}
