package org.yan.mongo.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.yan.mongo.entity.demo.Teacher;
import org.yan.mongo.repository.TeacherRepository;

import java.util.List;

/**
 * @author wangx 2020/6/8 17:05
 */
@Service
@AllArgsConstructor
public class TeacherService {
    private final TeacherRepository teacherRepository;


    public Teacher save(Teacher teacher) {

        return teacherRepository.save(teacher);
    }

    public List<Teacher> listAll() {
        return teacherRepository.findAll();
    }

    public void delete(String id) {
        teacherRepository.deleteById(id);
    }
}
