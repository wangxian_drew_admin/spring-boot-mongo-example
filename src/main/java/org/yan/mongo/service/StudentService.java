package org.yan.mongo.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.yan.mongo.entity.demo.Student;
import org.yan.mongo.repository.StudentRepository;

import java.util.List;

/**
 * @author wangx 2020/6/8 16:51
 */
@Service
@AllArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;


    public Student save(Student student) {
        return studentRepository.save(student);

    }

    public List<Student> listAll() {

        return studentRepository.findAll();
    }

}
