package org.yan.mongo.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.FeatureSource;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.geojson.feature.FeatureJSON;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.MultiPolygon;
import org.opengis.feature.Property;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.yan.mongo.entity.shp.BaseShapeModel;
import org.yan.mongo.entity.shp.GeometryModel;
import org.yan.mongo.entity.shp.MultiPolygonShapeModel;
import org.yan.mongo.entity.shp.PointShapeModel;

import java.io.File;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * shp工具类
 *
 * @author wangx 2020/06/14 22:06
 */
@Slf4j
public class ShapeToolUtils {

    private static ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private ShapeToolUtils() {

    }

    public static List<BaseShapeModel> getShapeFile(File file) throws Exception {

        Map<String, Object> map = new HashMap<>(1);
//        URL url = new URL("https://mcp-1300612569.cos.ap-shanghai.myqcloud.com/linkeda/%E4%BA%8C%E7%B1%BB%E8%B0%83%E6%9F%A5%E6%95%B0%E6%8D%AE.shp");
        map.put("url", file.toURI().toURL());
        ShapefileDataStore dataStore = (ShapefileDataStore) DataStoreFinder.getDataStore(map);
        //字符转码，防止中文乱码
        dataStore.setCharset(Charset.forName("GBK"));
        String typeName = dataStore.getTypeNames()[0];
        FeatureSource<SimpleFeatureType, SimpleFeature> source = dataStore.getFeatureSource(typeName);
        FeatureCollection<SimpleFeatureType, SimpleFeature> collection = source.getFeatures();
        FeatureIterator<SimpleFeature> features = collection.features();
        List<BaseShapeModel> multiPolygonShapeModels = new ArrayList<>();
        while (features.hasNext()) {
            SimpleFeature feature = features.next();
            FeatureJSON featureJson = new FeatureJSON();
            String json = featureJson.toString(feature);
            if (feature.getDefaultGeometryProperty().getType().getName().toString().equalsIgnoreCase("point")) {
                PointShapeModel pointShapeModel = OBJECT_MAPPER.readValue(json, PointShapeModel.class);
                multiPolygonShapeModels.add(pointShapeModel);
            } else {
                MultiPolygonShapeModel multiPolygonShapeModel = OBJECT_MAPPER.readValue(json, MultiPolygonShapeModel.class);
                log.info("featureJson:{}", json);
                for (Property property : feature.getValue()) {
                    if ("the_geom".equals(property.getName().toString())) {

                        MultiPolygon multiPolygon = (MultiPolygon) property.getValue();
                        Coordinate[] coordinates = multiPolygon.getCoordinates();
                        GeometryModel geometryModel = multiPolygonShapeModel.getGeometry();
                        List<List<List<List<BigDecimal>>>> tmpCoordinates = new ArrayList<>();
                        List<List<List<BigDecimal>>> data = new ArrayList<>();
                        List<List<BigDecimal>> data1 = new ArrayList<>();
                        for (int i = 0; i < coordinates.length; i++) {
                            List<BigDecimal> data2 = new ArrayList<>();
                            Coordinate coordinate = coordinates[i];
                            data2.add(BigDecimal.valueOf(coordinate.getX()));
                            data2.add(BigDecimal.valueOf(coordinate.getY()));
                            data1.add(data2);
                        }
                        data.add(data1);
                        tmpCoordinates.add(data);
                        geometryModel.setCoordinates(tmpCoordinates);

                        multiPolygonShapeModel.setGeometry(geometryModel);

                        log.info("{}", multiPolygon);
                    }
                    log.info("type:{},name:{},xvalue:{}", property.getType(), property.getName(), property.getValue());
                }
                multiPolygonShapeModels.add(multiPolygonShapeModel);
            }


        }
        log.info("=====================================================================");
        log.info("model size:{}", multiPolygonShapeModels.size());
        return multiPolygonShapeModels;
    }

    public static void main(String[] args) throws Exception {
//        getShapeFile(new File("C:\\Users\\wangx\\Documents\\Dshubs\\省林业局Gis项目\\矢量数据\\shp\\二类调查数据.shp"));
//        getShapeFile(new File("C:\\Users\\wangx\\Documents\\Dshubs\\省林业局Gis项目\\矢量数据\\实验林场数据\\树高点数据.shp"));
        getShapeFile(new File("C:\\Users\\wangx\\Documents\\Dshubs\\省林业局Gis项目\\矢量数据\\实验林场数据\\冠幅面数据.shp"));
    }

}
